<%-- 
    Document   : AdminPage
    Created on : Jun 19, 2021, 3:12:03 PM
    Author     : nguye
--%>

<%@page import="dao.AccountDao"%>
<%@page import="entity.Translator"%>
<%@page import="entity.Book"%>
<%@page import="Thai.entity.Report"%>
<%@page import="java.util.List"%>
<%@page import="entity.Account"%>
<%@page import="dao.TranslatorDao"%>
<%@page import="dao.Thai.TReportDao"%>
<%@page import="dao.BookDao"%>
<%@page import="dao.Thai.TAccountDao"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link rel="stylesheet" href="asset/css/bootstrap.css" />
        <link rel="stylesheet" href="asset/css/publisher.css" />
        <title>Document</title>
        <%
            String checkSearch = (String) request.getAttribute("checkSearch");
//            String type = (String) request.getAttribute("type");
            AccountDao acc = new AccountDao();
            List<Account> accs = new TAccountDao().getAll();
            List<Translator> trans = new TranslatorDao().getAll();
        %>
    </head>
    <body>
        <div class="wrapper">
            <!-- Sidebar  -->
            <nav id="sidebar">
                <div class="sidebar-header"  style="background: #fbd593;">
                    <a href="PublisherAddChapter}"><h3>BROMICS</h3></a>
                </div>

                <ul class="list-unstyled components" style="background: #FDE8C4;">
                    <li>
                        <a href="#">Dashboard</a>
                    </li>
                    <li>
                        <a href="PublisherAddChapter">Book Management</a>
                    </li>
                </ul>
            </nav>

            <!-- Page Content  -->
            <div id="content">
                <nav class="navbar navbar-expand-lg navbar-light bg-light">
                    <div class="container-fluid">
                        <button type="button" id="sidebarCollapse" class="btn btn-info"  style="background: #FDCBC4; border-color: #FDC4F5">
                            <i class="fas fa-align-left"></i>
                            <span>Toggle Sidebar</span>
                        </button>
                        <form class="d-flex" action="PublisherSearch">
                            <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search" name="searchValue">
                            <button class="btn btn-outline-dark" type="submit">Search</button>
                        </form>
                        <button
                            class="btn btn-dark d-inline-block d-lg-none ml-auto"
                            type="button"
                            data-toggle="collapse"
                            data-target="#navbarSupportedContent"
                            aria-controls="navbarSupportedContent"
                            aria-expanded="false"
                            aria-label="Toggle navigation"
                            >
                            <i class="fas fa-align-justify"></i>
                        </button>

                    </div>
                </nav>

                <button type="button" class="btn btn-outline-warning" onclick="myFunction2()">Add New Chapter</button>

                <div id="myModal2" class="hmodal" style="text-align: left;">
                    <!-- Modal content -->
                    <div class="hmodal-content">
                        <div class="hmodal-header">
                            <span class="close3">&times;</span>
                            <h2>Modal Header</h2>
                        </div>
                        <form action="AddChapterServlet">
                            <div class="hmodal-body">
                                <p style="display: inline;">Comic: </p>

                                <select name = "bookId" class="form-control" id="exampleFormControlSelect1"  style="display: inline; width: 50%">
                                    <c:forEach items="${requestScope.lsBook}" var="book">
                                        <option value="${book.id}">${book.name}</option>
                                    </c:forEach>
                                </select>
                                <p style="display: block;"></p>
                                <p style="display: inline;">Chapter: </p><input id="b2" name="chapterName">
                                <p></p>
                            </div>
                            <div class="hmodal-footer">
                                <button class="btn btn-outline-dark" type="submit" id="savebtn">Add</button>
                            </div>
                        </form>
                    </div>
                </div>

                <button type="button" class="btn btn-outline-warning" onclick="myFunction4()">Add New Comic</button>
                <div id="myModal4" class="hmodal" style="text-align: left;">
                    <!-- Modal content -->
                    <div class="hmodal-content">
                        <div class="hmodal-header">
                            <span class="close4">&times;</span>
                            <h2>Modal Header</h2>
                        </div>
                        <form action="EditServlet/Book">
                            <div class="hmodal-body">
                                <p style="display: inline;">Name: </p><input id="b3" name="name">
                                <p style="display: block;"></p>
                                <p style="display: inline;">Description: </p><textarea id="i3" name="description" style="width: 50%; height: 100px;"> </textarea>
                                <p style="display: block;"></p>
                                <p style="display: inline;">Image Name: </p><input id="k3"  name="img">
                                <input id="u1" hidden name="senderURL">
                                <p></p>
                            </div>
                            <div class="hmodal-footer">
                                <button class="btn btn-outline-light" type="submit" id="savebtn" style="margin: 15px">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
                <script>
// Get the modal
                    var modal2 = document.getElementById("myModal2");

// Get the <span> element that closes the modal
                    var span2 = document.getElementsByClassName("close2")[0];

                    var btn = document.getElementById("savebtn");

// When the user clicks the button, open the modal 

                    function myFunction2(a, b, c, d, e) {
                        modal2.style.display = "block";

                    }

// When the user clicks on <span> (x), close the modal

                    span2.onclick = function () {
                        modal2.style.display = "none";
                    }

                    btn.onclick = function () {
                        modal2.style.display = "none";
                    }

// When the user clicks anywhere outside of the modal, close it
                    window.onclick = function (event) {
                        if (event.target == modal2) {
                            modal.style.display = "none";
                            modal2.style.display = "none";
                        }
                    }
                </script>
                <% List<Book> list = null;
                    if (checkSearch.equalsIgnoreCase("list")) {
                        list = (List<Book>) request.getAttribute("lsBook");
                    } else if (checkSearch.equalsIgnoreCase("searchValue")) {
                        list = (List<Book>) request.getAttribute("searchList");
                    }

                %>

                <table class="table table-striped table-hover">
                    <thead>
                        <tr>
                            <th scope="col">BookID</th>
                            <th scope="col">Name</th>
                            <th scope="col">AuthorID</th>
                            <th scope="col">TranslatorID</th>
                            <th scope="col">TotalChap</th>
                            <th scope="col">Appear</th>
                            <th scope="col">TotalView</th>
                            <th scope="col">Status</th>
                            <th scope="col">ImgName</th>
                        </tr>
                    <tbody>
                        <% for (int i = 0; i < list.size(); i++) {
                        %>
                        <tr>
                            <th scope="row" onclick="myFunction('<%=list.get(i).getId()%>', '<%=list.get(i).getName()%>', '<%=list.get(i).getAuthorId()%>', '<%=list.get(i).getTranslatorId()%>', '<%=list.get(i).getTotalChap()%>', '<%=list.get(i).getAppear()%>', '<%=list.get(i).getTotalView()%>', '<%=list.get(i).getStatus()%>', '<%=list.get(i).getDescription()%>', '<%=list.get(i).getImgName()%>')">
                                <%=list.get(i).getId()%>
                                </td> 
                            <td onclick="myFunction('<%=list.get(i).getId()%>', '<%=list.get(i).getName()%>', '<%=list.get(i).getAuthorId()%>', '<%=list.get(i).getTranslatorId()%>', '<%=list.get(i).getTotalChap()%>', '<%=list.get(i).getAppear()%>', '<%=list.get(i).getTotalView()%>', '<%=list.get(i).getStatus()%>', '<%=list.get(i).getDescription()%>', '<%=list.get(i).getImgName()%>')">
                                <%=list.get(i).getName()%>
                            </td>
                            <td onclick="myFunction('<%=list.get(i).getId()%>', '<%=list.get(i).getName()%>', '<%=list.get(i).getAuthorId()%>', '<%=list.get(i).getTranslatorId()%>', '<%=list.get(i).getTotalChap()%>', '<%=list.get(i).getAppear()%>', '<%=list.get(i).getTotalView()%>', '<%=list.get(i).getStatus()%>', '<%=list.get(i).getDescription()%>', '<%=list.get(i).getImgName()%>')">
                                <%=list.get(i).getAuthorId()%>
                            </td>                
                            <td onclick="myFunction('<%=list.get(i).getId()%>', '<%=list.get(i).getName()%>', '<%=list.get(i).getAuthorId()%>', '<%=list.get(i).getTranslatorId()%>', '<%=list.get(i).getTotalChap()%>', '<%=list.get(i).getAppear()%>', '<%=list.get(i).getTotalView()%>', '<%=list.get(i).getStatus()%>', '<%=list.get(i).getDescription()%>', '<%=list.get(i).getImgName()%>')">
                                <%=list.get(i).getTranslatorId()%>
                            </td>
                            <td onclick="myFunction('<%=list.get(i).getId()%>', '<%=list.get(i).getName()%>', '<%=list.get(i).getAuthorId()%>', '<%=list.get(i).getTranslatorId()%>', '<%=list.get(i).getTotalChap()%>', '<%=list.get(i).getAppear()%>', '<%=list.get(i).getTotalView()%>', '<%=list.get(i).getStatus()%>', '<%=list.get(i).getDescription()%>', '<%=list.get(i).getImgName()%>')">
                                <%=list.get(i).getTotalChap()%>
                            </td>
                            <td onclick="myFunction('<%=list.get(i).getId()%>', '<%=list.get(i).getName()%>', '<%=list.get(i).getAuthorId()%>', '<%=list.get(i).getTranslatorId()%>', '<%=list.get(i).getTotalChap()%>', '<%=list.get(i).getAppear()%>', '<%=list.get(i).getTotalView()%>', '<%=list.get(i).getStatus()%>', '<%=list.get(i).getDescription()%>', '<%=list.get(i).getImgName()%>')">
                                <%=list.get(i).getAppear()%>
                            </td>
                            <td onclick="myFunction('<%=list.get(i).getId()%>', '<%=list.get(i).getName()%>', '<%=list.get(i).getAuthorId()%>', '<%=list.get(i).getTranslatorId()%>', '<%=list.get(i).getTotalChap()%>', '<%=list.get(i).getAppear()%>', '<%=list.get(i).getTotalView()%>', '<%=list.get(i).getStatus()%>', '<%=list.get(i).getDescription()%>', '<%=list.get(i).getImgName()%>')">
                                <%=list.get(i).getTotalView()%>
                            </td>
                            <td onclick="myFunction('<%=list.get(i).getId()%>', '<%=list.get(i).getName()%>', '<%=list.get(i).getAuthorId()%>', '<%=list.get(i).getTranslatorId()%>', '<%=list.get(i).getTotalChap()%>', '<%=list.get(i).getAppear()%>', '<%=list.get(i).getTotalView()%>', '<%=list.get(i).getStatus()%>', '<%=list.get(i).getDescription()%>', '<%=list.get(i).getImgName()%>')">
                                <%=list.get(i).getStatus()%>
                            </td>
                            <td onclick="myFunction('<%=list.get(i).getId()%>', '<%=list.get(i).getName()%>', '<%=list.get(i).getAuthorId()%>', '<%=list.get(i).getTranslatorId()%>', '<%=list.get(i).getTotalChap()%>', '<%=list.get(i).getAppear()%>', '<%=list.get(i).getTotalView()%>', '<%=list.get(i).getStatus()%>', '<%=list.get(i).getDescription()%>', '<%=list.get(i).getImgName()%>')">
                                <img src="asset/img/book/\<%=list.get(i).getImgName()%>" alt="" width="180px" />
                            </td>
                            <td>
                                <a onclick="myFunction3('<%=list.get(i).getId()%>', '<%=list.get(i).getName()%>', '<%=list.get(i).getAuthorId()%>', '<%=list.get(i).getTranslatorId()%>', '<%=list.get(i).getTotalChap()%>', '<%=list.get(i).getAppear()%>', '<%=list.get(i).getTotalView()%>', '<%=list.get(i).getStatus()%>', '<%=list.get(i).getDescription()%>', '<%=list.get(i).getImgName()%>')">Edit</a>
                                <button type="button" class="btn btn-outline-warning" onclick="myFunction2('<%=list.get(i).getId()%>', '<%=list.get(i).getName()%>')">Add Chapter</button>
                            </td> 
                        </tr>
                    <div id="myModal" class="hmodal" style="text-align: left;">
                        <!-- Modal content -->
                        <div class="hmodal-content">
                            <div class="hmodal-header">
                                <span class="close">&times;</span>
                                <h2>Book</h2>
                            </div>
                            <div class="hmodal-body">
                                <p id="a"></p>
                                <p id="b"></p>
                                <p id="c"></p>
                                <p id="d"></p>
                                <p id="e"></p>
                                <p id="f"></p>
                                <p id="g"></p>
                                <p id="h"></p>
                                <p id="i"></p>
                                <p id="k"></p>
                            </div>
                            <div class="hmodal-footer">
                                <h3></h3>
                            </div>
                        </div>
                    </div>

                    <div id="myModal3" class="hmodal" style="text-align: left;">
                        <!-- Modal content -->
                        <div class="hmodal-content">
                            <div class="hmodal-header">
                                <span class="close2">&times;</span>
                                <h2>Book</h2>
                            </div>
                            <form action="EditServlet/Book">
                                <div class="hmodal-body">
                                    <p style="display: inline;">ID: </p><input readonly="true" id="a2" name="id" >
                                    <p style="display: block;"></p>
                                    <p style="display: inline;">Name: </p><input id="b2" name="name">
                                    <p style="display: block;"></p>
                                    <p style="display: inline;">Author ID: </p><input id="c2" name="aid">
                                    <p style="display: block;"></p>
                                    <p style="display: inline;">Translator ID: </p><input id="d2" name="tid">
                                    <p style="display: block;"></p>
                                    <p style="display: inline;">Total Chapter: </p><input readonly="true" id="e2" name="total">
                                    <p style="display: block;"></p>
                                    <p style="display: inline;">Appear: </p><input id="f2" name="appear">
                                    <p style="display: block;"></p>
                                    <p style="display: inline;">Total View: </p><input id="g2" name="view">
                                    <p style="display: block;"></p>
                                    <p style="display: inline;">Status: </p><input id="h2" name="status">
                                    <p style="display: block;"></p>
                                    <p style="display: inline;">Description: </p><textarea id="i2" name="description" style="width: 50%; height: 100px;"> </textarea>
                                    <p style="display: block;"></p>
                                    <p style="display: inline;">Image Name: </p><input id="k2"  name="img">
                                    <input id="u1" hidden name="senderURL">
                                    <p></p>
                                </div>
                                <div class="hmodal-footer">
                                    <button class="btn btn-outline-light" type="submit" id="savebtn" style="margin: 15px">Save</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <%}%>
                    </tbody>
                </table>
                <script>
// Get the modal
                    var modal = document.getElementById("myModal");
                    var modal2 = document.getElementById("myModal2");
                    var modal3 = document.getElementById("myModal3");
                    var modal4 = document.getElementById("myModal4");
// Get the <span> element that closes the modal
                    var span = document.getElementsByClassName("close")[0];
                    var span2 = document.getElementsByClassName("close2")[0];
                    var span3 = document.getElementsByClassName("close3")[0];
                    var span4 = document.getElementsByClassName("close4")[0];
                    var btn = document.getElementById("savebtn");

// When the user clicks the button, open the modal 
                    function myFunction(a, b, c, d, e, f, g, h, i, k) {
                        modal.style.display = "block";
                        document.getElementById("a").innerHTML = "BookID: " + a;
                        document.getElementById("b").innerHTML = "Name: " + b;
                        document.getElementById("c").innerHTML = "AuthorID: " + c;
                        document.getElementById("d").innerHTML = "TranslatorID: " + d;
                        document.getElementById("e").innerHTML = "TotalChap: " + e;
                        document.getElementById("f").innerHTML = "Appear: " + f;
                        document.getElementById("g").innerHTML = "TotalView: " + g;
                        document.getElementById("h").innerHTML = "Status: " + h;
                        document.getElementById("i").innerHTML = "Description: " + i;
                        document.getElementById("k").innerHTML = "ImgName: " + k;

                    }
                    function myFunction2(a, b) {
                        modal2.style.display = "block";
                        document.getElementById("exampleFormControlSelect1").value = a;
                    }

                    function myFunction3(a, b, c, d, e, f, g, h, i, k) {
                        modal3.style.display = "block";
                        document.getElementById("a2").value = a;
                        document.getElementById("b2").value = b;
                        document.getElementById("c2").value = c;
                        document.getElementById("d2").value = d;
                        document.getElementById("e2").value = e;
                        document.getElementById("f2").value = f;
                        document.getElementById("g2").value = g;
                        document.getElementById("h2").value = h;
                        document.getElementById("i2").value = i;
                        document.getElementById("k2").value = k;
                        document.getElementById("u1").value = window.location.href;
                    }
                    function myFunction4() {
                        modal4.style.display="block";
                    }


// When the user clicks on <span> (x), close the modal
                    span.onclick = function () {
                        modal.style.display = "none";
                    }

                    span2.onclick = function () {
                        modal3.style.display = "none";
                    }
                    span3.onclick = function () {
                        modal2.style.display = "none";
                    }
                    span4.onclick = function () {
                        modal4.style.display = "none";
                    }


                    btn.onclick = function () {
                        modal2.style.display = "none";
                    }

// When the user clicks anywhere outside of the modal, close it
                    window.onclick = function (event) {
                        if (event.target == modal || event.target == modal2) {
                            modal.style.display = "none";
                            modal2.style.display = "none";
                        }
                    }

                </script>
            </div>
        </div>
        <script src="https://code.jquery.com/jquery-3.4.1.js"></script>
        <script src="asset/js/bootstrap.js"></script>
        <script>
                    $(document).ready(function () {
                        $("#sidebarCollapse").on("click", function () {
                            $("#sidebar").toggleClass("active");
                        });
                    });


        </script>
    </body>
</html>