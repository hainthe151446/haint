package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class adminStatisticPage_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("<!doctype html>\n");
      out.write("<html>\n");
      out.write("<head>\n");
      out.write("    <title>Makisu ~ CSS 3D Dropdown Concept</title>\n");
      out.write("    <link href='http://fonts.googleapis.com/css?family=Days+One' rel='stylesheet' type='text/css'>\n");
      out.write("    <link rel=\"stylesheet\" href=\"asset/css/adminStatisticPage\">\n");
      out.write("</head>\n");
      out.write("<body>\n");
      out.write("\n");
      out.write("    <header class=\"header\">\n");
      out.write("        <hgroup>\n");
      out.write("            <h1>Makisu</h1>\n");
      out.write("            <h2>CSS 3D Dropdown Concept</h2>\n");
      out.write("        </hgroup>\n");
      out.write("    </header>\n");
      out.write("\n");
      out.write("    <section class=\"demo\">\n");
      out.write("\n");
      out.write("        <dl class=\"list nigiri\">\n");
      out.write("            <dt>Nigiri</dt>\n");
      out.write("            <dd><a href=\"#\">Maguro</a></dd>\n");
      out.write("            <dd><a href=\"#\">Sake</a></dd>\n");
      out.write("            <dd><a href=\"#\">Unagi</a></dd>\n");
      out.write("            <dd><a href=\"#\">Buri</a></dd>\n");
      out.write("            <dd><a href=\"#\">Suzuki</a></dd>\n");
      out.write("            <dd><a href=\"#\">Saba</a></dd>\n");
      out.write("            <dd><a href=\"#\">Iwashi</a></dd>\n");
      out.write("            <dd><a href=\"#\">Kohada</a></dd>\n");
      out.write("            <dd><a href=\"#\">Hirame</a></dd>\n");
      out.write("            <dd><a href=\"#\">Tobiwo</a></dd>\n");
      out.write("        </dl>\n");
      out.write("\n");
      out.write("        <dl class=\"list maki\">\n");
      out.write("            <dt>Maki</dt>\n");
      out.write("            <dd><a href=\"#\">Ana-kyu</a></dd>\n");
      out.write("            <dd><a href=\"#\">Chutoro</a></dd>\n");
      out.write("            <dd><a href=\"#\">Kaiware</a></dd>\n");
      out.write("            <dd><a href=\"#\">Kampyo</a></dd>\n");
      out.write("            <dd><a href=\"#\">Kappa</a></dd>\n");
      out.write("            <dd><a href=\"#\">Natto</a></dd>\n");
      out.write("            <dd><a href=\"#\">Negitoro</a></dd>\n");
      out.write("            <dd><a href=\"#\">Oshinko</a></dd>\n");
      out.write("            <dd><a href=\"#\">Otoro</a></dd>\n");
      out.write("            <dd><a href=\"#\">Tekka</a></dd>\n");
      out.write("        </dl>\n");
      out.write("\n");
      out.write("        <dl class=\"list sashimi\">\n");
      out.write("            <dt>Sashimi</dt>\n");
      out.write("            <dd><a href=\"#\">Maguro</a></dd>\n");
      out.write("            <dd><a href=\"#\">Toro</a></dd>\n");
      out.write("            <dd><a href=\"#\">Ebi</a></dd>\n");
      out.write("            <dd><a href=\"#\">Saba</a></dd>\n");
      out.write("            <dd><a href=\"#\">Ika</a></dd>\n");
      out.write("            <dd><a href=\"#\">Tako</a></dd>\n");
      out.write("            <dd><a href=\"#\">Tomago</a></dd>\n");
      out.write("            <dd><a href=\"#\">Kani</a></dd>\n");
      out.write("            <dd><a href=\"#\">Katsuo</a></dd>\n");
      out.write("            <dd><a href=\"#\">Maguro</a></dd>\n");
      out.write("        </dl>\n");
      out.write("\n");
      out.write("        <a href=\"#\" class=\"toggle\">Toggle</a>\n");
      out.write("\n");
      out.write("    </section>\n");
      out.write("\n");
      out.write("    <div class=\"warning\">\n");
      out.write("        <div class=\"message\">\n");
      out.write("            <h1>CSS 3D Not Detected :(</h1>\n");
      out.write("            <p>I couldn't detect your browser's CSS 3D capabilities. If I'm wrong, please <a href=\"https://github.com/soulwire/Makisu/issues\" target=\"_blank\">file an issue</a>, otherwise, try a <a href=\"http://www.google.com/chrome\" target=\"_blank\">sexier browser</a></p>\n");
      out.write("        </div>\n");
      out.write("    </div>\n");
      out.write("\n");
      out.write("    <a href=\"https://github.com/soulwire/Makisu\" target=\"_blank\"><img style=\"position: absolute; top: 0; right: 0; border: 0;\" src=\"https://s3.amazonaws.com/github/ribbons/forkme_right_darkblue_121621.png\" alt=\"Fork me on GitHub\"></a>\n");
      out.write("\n");
      out.write("    <script src=\"http://ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js\"></script>\n");
      out.write("    <script src=\"asset/js/adminStatisticPage.js\"></script>\n");
      out.write("    <script>\n");
      out.write("\n");
      out.write("        // The `enabled` flag will be `false` if CSS 3D isn't available\n");
      out.write("\n");
      out.write("        if ( $.fn.makisu.enabled ) {\n");
      out.write("\n");
      out.write("            var $sashimi = $( '.sashimi' );\n");
      out.write("            var $nigiri = $( '.nigiri' );\n");
      out.write("            var $maki = $( '.maki' );\n");
      out.write("\n");
      out.write("            // Create Makisus\n");
      out.write("\n");
      out.write("            $nigiri.makisu({\n");
      out.write("                selector: 'dd',\n");
      out.write("                overlap: 0.85,\n");
      out.write("                speed: 1.7\n");
      out.write("            });\n");
      out.write("\n");
      out.write("            $maki.makisu({\n");
      out.write("                selector: 'dd',\n");
      out.write("                overlap: 0.6,\n");
      out.write("                speed: 0.85\n");
      out.write("            });\n");
      out.write("\n");
      out.write("            $sashimi.makisu({\n");
      out.write("                selector: 'dd',\n");
      out.write("                overlap: 0.2,\n");
      out.write("                speed: 0.5\n");
      out.write("            });\n");
      out.write("\n");
      out.write("            // Open all\n");
      out.write("            \n");
      out.write("            $( '.list' ).makisu( 'open' );\n");
      out.write("\n");
      out.write("            // Toggle on click\n");
      out.write("\n");
      out.write("            $( '.toggle' ).on( 'click', function() {\n");
      out.write("                $( '.list' ).makisu( 'toggle' );\n");
      out.write("            });\n");
      out.write("\n");
      out.write("            // Disable all links\n");
      out.write("\n");
      out.write("            $( '.demo a' ).click( function( event ) {\n");
      out.write("                event.preventDefault();\n");
      out.write("            });\n");
      out.write("\n");
      out.write("        } else {\n");
      out.write("\n");
      out.write("            $( '.warning' ).show();\n");
      out.write("        }\n");
      out.write("\n");
      out.write("    </script>\n");
      out.write("</body>\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
