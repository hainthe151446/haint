package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class bookDetail_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  static {
    _jspx_dependants = new java.util.ArrayList<String>(2);
    _jspx_dependants.add("/navbar.jsp");
    _jspx_dependants.add("/comment.jsp");
  }

  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_c_forEach_var_items;
  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_c_choose;
  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_c_otherwise;
  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_c_when_test;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _jspx_tagPool_c_forEach_var_items = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _jspx_tagPool_c_choose = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _jspx_tagPool_c_otherwise = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _jspx_tagPool_c_when_test = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
  }

  public void _jspDestroy() {
    _jspx_tagPool_c_forEach_var_items.release();
    _jspx_tagPool_c_choose.release();
    _jspx_tagPool_c_otherwise.release();
    _jspx_tagPool_c_when_test.release();
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\r\n");
      out.write("<!DOCTYPE html>\r\n");
      out.write("<html lang=\"en\">\r\n");
      out.write("    <head>\r\n");
      out.write("        <meta charset=\"UTF-8\" />\r\n");
      out.write("        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\" />\r\n");
      out.write("        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" />\r\n");
      out.write("        <link rel=\"stylesheet\" href=\"asset/css/bootstrap.css\" />\r\n");
      out.write("        <link rel=\"stylesheet\" href=\"asset/css/style.css\" />\r\n");
      out.write("        <link\r\n");
      out.write("            rel=\"stylesheet\"\r\n");
      out.write("            href=\"https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css\"\r\n");
      out.write("            />\r\n");
      out.write("\r\n");
      out.write("        <title>BookDetails</title>\r\n");
      out.write("    </head>\r\n");
      out.write("    <style>\r\n");
      out.write("        .book-report .dropdown-item:hover{\r\n");
      out.write("            cursor:pointer;\r\n");
      out.write("        }\r\n");
      out.write("    </style>\r\n");
      out.write("    <body>\r\n");
      out.write("        <div class=\"container-fluid\">\r\n");
      out.write("            ");
      out.write("\r\n");
      out.write("<header>\r\n");
      out.write("    <div class=\"header-wrapper\">\r\n");
      out.write("        <div class=\"left\">\r\n");
      out.write("            <a href=\"BookServlet\">\r\n");
      out.write("                <h1>BROMICS</h1>\r\n");
      out.write("            </a>\r\n");
      out.write("            <div class=\"menu\">\r\n");
      out.write("                <ul>\r\n");
      out.write("                    <li><a href=\"BookServlet\" class=\"off\">Home</a></li>\r\n");
      out.write("                    <li class=\"dropdown\">\r\n");
      out.write("                        <a href=\"\" class=\"dropdown-toggle off\" data-toggle=\"dropdown\"\r\n");
      out.write("                           >Categories</a\r\n");
      out.write("                        >\r\n");
      out.write("                        <ul class=\"dropdown-menu container\">\r\n");
      out.write("                            <div class=\"row\">\r\n");
      out.write("                                ");
      if (_jspx_meth_c_forEach_0(_jspx_page_context))
        return;
      out.write("\r\n");
      out.write("                            </div>\r\n");
      out.write("                        </ul>\r\n");
      out.write("                    </li>\r\n");
      out.write("                    <li class=\"off\">\r\n");
      out.write("                        <a href=\"BookFilterServlet\" class=\"off\">Explore</a>\r\n");
      out.write("                    </li>\r\n");
      out.write("                    <li>\r\n");
      out.write("                        <a onclick=\"toggleNotification()\"\r\n");
      out.write("                           ><div class=\"btn-notification\" style=\"position: absolute; margin-left: 17%\">\r\n");
      out.write("                                <i class=\"fa fa-bell\" style=\"font-size: 30px; color: white;\"></i></div\r\n");
      out.write("                            ></a>\r\n");
      out.write("                        <div class=\"notifi-box\" id=\"box\">\r\n");
      out.write("                            <h2>Notification</h2>\r\n");
      out.write("                            ");
      if (_jspx_meth_c_forEach_1(_jspx_page_context))
        return;
      out.write("\r\n");
      out.write("                            \r\n");
      out.write("                        </div>\r\n");
      out.write("                    </li>\r\n");
      out.write("                </ul>\r\n");
      out.write("            </div>\r\n");
      out.write("\r\n");
      out.write("        </div>\r\n");
      out.write("        <div class=\"right\">\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("            <script>\r\n");
      out.write("                var box = document.getElementById('box');\r\n");
      out.write("                var down = false;\r\n");
      out.write("\r\n");
      out.write("                function toggleNotification() {\r\n");
      out.write("                    if (down) {\r\n");
      out.write("                        box.style.height = '0px';\r\n");
      out.write("                        box.style.opacity = 0;\r\n");
      out.write("                        down = false;\r\n");
      out.write("                    } else {\r\n");
      out.write("                        box.style.height = '510px';\r\n");
      out.write("                        box.style.opacity = 1;\r\n");
      out.write("                        down = true;\r\n");
      out.write("                    }\r\n");
      out.write("                }\r\n");
      out.write("            </script>\r\n");
      out.write("\r\n");
      out.write("            <form\r\n");
      out.write("                action=\"BookSearchServlet\"\r\n");
      out.write("                method=\"GET\"\r\n");
      out.write("                class=\"navbar-form form-inline\"\r\n");
      out.write("                style=\"display: inline-block\"\r\n");
      out.write("                >\r\n");
      out.write("                <div class=\"input-group search-box\">\r\n");
      out.write("                    <input\r\n");
      out.write("                        type=\"text\"\r\n");
      out.write("                        id=\"search\"\r\n");
      out.write("                        class=\"form-control\"\r\n");
      out.write("                        placeholder=\"Search for book\"\r\n");
      out.write("                        name=\"search\"\r\n");
      out.write("                        />\r\n");
      out.write("                    <button class=\"input-group-addon\" type=\"submit\">\r\n");
      out.write("                        <i class=\"fa fa-search\" style=\"font-size: 27px\"></i>\r\n");
      out.write("                    </button>\r\n");
      out.write("                </div>\r\n");
      out.write("            </form>\r\n");
      out.write("            ");
      if (_jspx_meth_c_choose_0(_jspx_page_context))
        return;
      out.write("\r\n");
      out.write("\r\n");
      out.write("            <!-- <div class=\"section_search\"></div> -->\r\n");
      out.write("        </div>\r\n");
      out.write("    </div>\r\n");
      out.write("    <script src=\"asset/js/popper.js\"></script>\r\n");
      out.write("    <script type=\"text/javascript\">\r\n");
      out.write("                $(\".off\").click(function () {\r\n");
      out.write("                    $(this).toggleClass(\"on\");\r\n");
      out.write("                });\r\n");
      out.write("    </script>\r\n");
      out.write("</header>\r\n");
      out.write("\r\n");
      out.write("            <div class=\"bookDetail-wrapper\">\r\n");
      out.write("                <div class=\"bookDetail-section\">\r\n");
      out.write("                    <div class=\"bookDetail\">\r\n");
      out.write("                        <div class=\"bookDetail-img\">\r\n");
      out.write("                            <img\r\n");
      out.write("                                src=\"asset/img/book/");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${requestScope.book.imgName}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("\"\r\n");
      out.write("                                alt=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${requestScope.book.id}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("\"\r\n");
      out.write("                                />\r\n");
      out.write("                        </div>\r\n");
      out.write("                        <div class=\"bookDetail-information\">\r\n");
      out.write("                            <div class=\"book-name\">\r\n");
      out.write("                                <h1>");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${requestScope.book.name}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("</h1>\r\n");
      out.write("                            </div>\r\n");
      out.write("                            <div class=\"infor\">\r\n");
      out.write("                                <div>\r\n");
      out.write("                                    <span class=\"book-tag\"\r\n");
      out.write("                                          ><i class=\"fa fa-rss\"></i> Status:</span\r\n");
      out.write("                                    >\r\n");
      out.write("                                    <span class=\"tag-detail book-status\"\r\n");
      out.write("                                          >");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${requestScope.book.status}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("</span\r\n");
      out.write("                                    >\r\n");
      out.write("                                </div>\r\n");
      out.write("                                <div>\r\n");
      out.write("                                    <span class=\"book-tag\">\r\n");
      out.write("                                        <i class=\"fa fa-tag\"></i> Category:</span\r\n");
      out.write("                                    >\r\n");
      out.write("                                    <span class=\"tag-detail\">\r\n");
      out.write("                                        ");
      if (_jspx_meth_c_forEach_2(_jspx_page_context))
        return;
      out.write("\r\n");
      out.write("                                    </span>\r\n");
      out.write("                                </div>\r\n");
      out.write("                                <div>\r\n");
      out.write("                                    <span class=\"book-tag\"><i class=\"fa fa-eye\"></i> View:</span>\r\n");
      out.write("                                    <span class=\"tag-detail\">");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${requestScope.book.totalView}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("</span>\r\n");
      out.write("                                </div>\r\n");
      out.write("                                <div class=\"book-tag wrap-star\">\r\n");
      out.write("                                    <div class=\"star-wrapper\">\r\n");
      out.write("                                        <a\r\n");
      out.write("                                            href=\"#\"\r\n");
      out.write("                                            onclick=\"rate(");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${sessionScope.accountId}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write(',');
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${requestScope.book.id}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write(", 5)\"\r\n");
      out.write("                                            class=\"fa fa-star s1\"\r\n");
      out.write("                                            ></a>\r\n");
      out.write("                                        <a\r\n");
      out.write("                                            href=\"#\"\r\n");
      out.write("                                            onclick=\"rate(");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${sessionScope.accountId}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write(',');
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${requestScope.book.id}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write(", 4)\"\r\n");
      out.write("                                            class=\"fa fa-star s2\"\r\n");
      out.write("                                            ></a>\r\n");
      out.write("                                        <a\r\n");
      out.write("                                            href=\"#\"\r\n");
      out.write("                                            onclick=\"rate(");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${sessionScope.accountId}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write(',');
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${requestScope.book.id}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write(", 3)\"\r\n");
      out.write("                                            class=\"fa fa-star s3\"\r\n");
      out.write("                                            ></a>\r\n");
      out.write("                                        <a\r\n");
      out.write("                                            href=\"#\"\r\n");
      out.write("                                            onclick=\"rate(");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${sessionScope.accountId}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write(',');
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${requestScope.book.id}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write(", 2)\"\r\n");
      out.write("                                            class=\"fa fa-star s4\"\r\n");
      out.write("                                            ></a>\r\n");
      out.write("                                        <a\r\n");
      out.write("                                            href=\"#\"\r\n");
      out.write("                                            onclick=\"rate(");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${sessionScope.accountId}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write(',');
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${requestScope.book.id}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write(", 1)\"\r\n");
      out.write("                                            class=\"fa fa-star s5\"\r\n");
      out.write("                                            ></a>\r\n");
      out.write("                                        <span id=\"rate-mark\">");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${requestScope.rateMark}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("</span>\r\n");
      out.write("                                    </div>\r\n");
      out.write("                                    <script src=\"https://kit.fontawesome.com/5ea815c1d0.js\"></script>\r\n");
      out.write("                                </div>\r\n");
      out.write("                                <!--Rating-->\r\n");
      out.write("                            </div>\r\n");
      out.write("                            <div class=\"action\">\r\n");
      out.write("                                <div class=\"action-1\">\r\n");
      out.write("                                    <button\r\n");
      out.write("                                        onclick=\"follow2(");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${book.id}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write(',');
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${sessionScope.accountId}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write(", 'book-bookmark', 'red', 'black')\"\r\n");
      out.write("                                        class=\"action-follow\"\r\n");
      out.write("                                        >\r\n");
      out.write("                                        <span id=\"book-bookmark");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${requestScope.book.id}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("\"\r\n");
      out.write("                                              ");
if ((boolean) request.getAttribute("isFollow")) {
      out.write("\r\n");
      out.write("                                              style=\"color:red\"");
}
      out.write(">Follow\r\n");
      out.write("                                        </span>\r\n");
      out.write("                                    </button>\r\n");
      out.write("\r\n");
      out.write("                                    <button type=\"button\" class=\"action-follow\" style=\"margin-left: 15px\" onclick=\"setClipboard()\">\r\n");
      out.write("                                        <i class=\"fas fa-share\"></i>\r\n");
      out.write("                                    </button>\r\n");
      out.write("\r\n");
      out.write("                                    <button type=\"button\" onclick=\"getURL();\">Get Page URL</button>\r\n");
      out.write("                                    <script>\r\n");
      out.write("                                        function setClipboard() {\r\n");
      out.write("                                            var tempInput = document.createElement(\"input\");\r\n");
      out.write("                                            tempInput.style = \"position: absolute; left: -1000px; top: -1000px\";\r\n");
      out.write("                                            tempInput.value = window.location.href;\r\n");
      out.write("                                            document.body.appendChild(tempInput);\r\n");
      out.write("                                            tempInput.select();\r\n");
      out.write("                                            document.execCommand(\"copy\");\r\n");
      out.write("                                            document.body.removeChild(tempInput);\r\n");
      out.write("                                        }\r\n");
      out.write("                                    </script>\r\n");
      out.write("                                    <button type=\"button\" class=\"btn btn-danger dropdown-toggle\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\r\n");
      out.write("                                        Report this book!\r\n");
      out.write("                                    </button>\r\n");
      out.write("                                    <div class=\"dropdown-menu book-report\">\r\n");
      out.write("                                        <div class=\"book-report-content\" style=\"display:none\"></div>\r\n");
      out.write("                                        <div class=\"dropdown-item\" >Abusive content</div>\r\n");
      out.write("                                        <div class=\"dropdown-item\" >Plagiarism</div>\r\n");
      out.write("                                        <div class=\"dropdown-item\" >Erotic content</div>\r\n");
      out.write("                                        <div class=\"dropdown-item\" >Hate speech</div>\r\n");
      out.write("\r\n");
      out.write("                                    </div>\r\n");
      out.write("                                </div>\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("                            </div>\r\n");
      out.write("                            <div class=\"action-2\">\r\n");
      out.write("                                <div class=\"read-first\">\r\n");
      out.write("                                    <a href=\"ReadFirstServlet?bookId=");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${requestScope.book.id}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("\">\r\n");
      out.write("                                        <button class=\"btn-hover first\" onclick=\"\">\r\n");
      out.write("                                            Read First\r\n");
      out.write("                                        </button>\r\n");
      out.write("                                    </a>\r\n");
      out.write("                                </div>\r\n");
      out.write("                                <div class=\"read-last\">\r\n");
      out.write("                                    <button class=\"btn-hover last\">Continue</button>\r\n");
      out.write("                                </div>\r\n");
      out.write("                            </div>\r\n");
      out.write("                        </div>\r\n");
      out.write("                    </div>\r\n");
      out.write("                    <div class=\"container\">\r\n");
      out.write("                        <details>\r\n");
      out.write("                            <summary>Summary</summary>\r\n");
      out.write("                            <p>");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${requestScope.book.description}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("</p>\r\n");
      out.write("                        </details>\r\n");
      out.write("                        <div class=\"chap-list-wrapper\">\r\n");
      out.write("                            <div class=\"function\">Filter for chapter here</div>\r\n");
      out.write("                            <div class=\"chap-list-detail\">\r\n");
      out.write("                                <table class=\"tablee\">\r\n");
      out.write("                                    <thead>\r\n");
      out.write("                                        <tr>\r\n");
      out.write("                                            <th>Chapter</th>\r\n");
      out.write("                                            <th>Rating</th>\r\n");
      out.write("                                            <th>View</th>\r\n");
      out.write("                                            <th>Last_Updated</th>\r\n");
      out.write("                                        </tr>\r\n");
      out.write("                                    </thead>\r\n");
      out.write("                                    <tbody>\r\n");
      out.write("                                        ");
      if (_jspx_meth_c_forEach_3(_jspx_page_context))
        return;
      out.write("\r\n");
      out.write("                                    </tbody>\r\n");
      out.write("                                </table>\r\n");
      out.write("                            </div>\r\n");
      out.write("                            ");
      out.write("<script src=\"asset/js/commentAjax.js\"></script>\r\n");
      out.write("<input style=\"display:none\" id=\"temp\" value=\"?bookId=");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${requestScope.book.id}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("&chapterId=");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${requestScope.chapter.chapterId}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("\">\r\n");
      out.write("<div class=\"comment-section container\">\r\n");
      out.write("    <div class=\"row\">\r\n");
      out.write("        <div class=\"col-md-12\">\r\n");
      out.write("            ");
      if (_jspx_meth_c_choose_1(_jspx_page_context))
        return;
      out.write("\r\n");
      out.write("        </div>\r\n");
      out.write("        ");
      if (_jspx_meth_c_forEach_4(_jspx_page_context))
        return;
      out.write("\r\n");
      out.write("        <style type=\"text/css\">\r\n");
      out.write("            @media (min-width: 0) {\r\n");
      out.write("                .g-mr-15 {\r\n");
      out.write("                    margin-right: 1.07143rem !important;\r\n");
      out.write("                }\r\n");
      out.write("            }\r\n");
      out.write("            @media (min-width: 0) {\r\n");
      out.write("                .g-mt-3 {\r\n");
      out.write("                    margin-top: 0.21429rem !important;\r\n");
      out.write("                }\r\n");
      out.write("            }\r\n");
      out.write("\r\n");
      out.write("            .g-height-50 {\r\n");
      out.write("                height: 50px;\r\n");
      out.write("            }\r\n");
      out.write("\r\n");
      out.write("            .g-width-50 {\r\n");
      out.write("                width: 50px !important;\r\n");
      out.write("            }\r\n");
      out.write("\r\n");
      out.write("            @media (min-width: 0) {\r\n");
      out.write("                .g-pa-30 {\r\n");
      out.write("                    padding: 2.14286rem !important;\r\n");
      out.write("                }\r\n");
      out.write("            }\r\n");
      out.write("\r\n");
      out.write("            .g-bg-secondary {\r\n");
      out.write("                background-color: #fafafa !important;\r\n");
      out.write("            }\r\n");
      out.write("\r\n");
      out.write("            .u-shadow-v18 {\r\n");
      out.write("                box-shadow: 0 5px 10px -6px rgba(0, 0, 0, 0.15);\r\n");
      out.write("                border: 0.5px solid gray;\r\n");
      out.write("            }\r\n");
      out.write("\r\n");
      out.write("            .g-color-gray-dark-v4 {\r\n");
      out.write("                color: #777 !important;\r\n");
      out.write("            }\r\n");
      out.write("\r\n");
      out.write("            .g-font-size-12 {\r\n");
      out.write("                font-size: 0.85714rem !important;\r\n");
      out.write("            }\r\n");
      out.write("\r\n");
      out.write("            .media-comment {\r\n");
      out.write("                margin-top: 20px;\r\n");
      out.write("            }\r\n");
      out.write("        </style>\r\n");
      out.write("    </div>\r\n");
      out.write("</div>\r\n");
      out.write("\r\n");
      out.write("                        </div>\r\n");
      out.write("                    </div>\r\n");
      out.write("                </div>\r\n");
      out.write("\r\n");
      out.write("            </div>\r\n");
      out.write("        </div>\r\n");
      out.write("    </div>\r\n");
      out.write("    <script src=\"https://code.jquery.com/jquery-3.4.1.js\"></script>\r\n");
      out.write("    <script src=\"asset/js/bootstrap.js\"></script>\r\n");
      out.write("    <script src=\"asset/js/bookInfo.js\"></script>\r\n");
      out.write("\r\n");
      out.write("</body>\r\n");
      out.write("</html>\r\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }

  private boolean _jspx_meth_c_forEach_0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:forEach
    org.apache.taglibs.standard.tag.rt.core.ForEachTag _jspx_th_c_forEach_0 = (org.apache.taglibs.standard.tag.rt.core.ForEachTag) _jspx_tagPool_c_forEach_var_items.get(org.apache.taglibs.standard.tag.rt.core.ForEachTag.class);
    _jspx_th_c_forEach_0.setPageContext(_jspx_page_context);
    _jspx_th_c_forEach_0.setParent(null);
    _jspx_th_c_forEach_0.setItems((java.lang.Object) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${requestScope.lsCategory}", java.lang.Object.class, (PageContext)_jspx_page_context, null));
    _jspx_th_c_forEach_0.setVar("category");
    int[] _jspx_push_body_count_c_forEach_0 = new int[] { 0 };
    try {
      int _jspx_eval_c_forEach_0 = _jspx_th_c_forEach_0.doStartTag();
      if (_jspx_eval_c_forEach_0 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
        do {
          out.write("\r\n");
          out.write("                                    <div class=\"col-md-2\">\r\n");
          out.write("                                        <li>\r\n");
          out.write("                                            <a\r\n");
          out.write("                                                class=\"dropdown-item\"\r\n");
          out.write("                                                href=\"BookWithCategoryServlet?categoryId=");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${category.categoryId}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("\"\r\n");
          out.write("                                                >");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${category.categoryName}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("</a\r\n");
          out.write("                                            >\r\n");
          out.write("                                        </li>\r\n");
          out.write("                                    </div>\r\n");
          out.write("                                ");
          int evalDoAfterBody = _jspx_th_c_forEach_0.doAfterBody();
          if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
            break;
        } while (true);
      }
      if (_jspx_th_c_forEach_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        return true;
      }
    } catch (Throwable _jspx_exception) {
      while (_jspx_push_body_count_c_forEach_0[0]-- > 0)
        out = _jspx_page_context.popBody();
      _jspx_th_c_forEach_0.doCatch(_jspx_exception);
    } finally {
      _jspx_th_c_forEach_0.doFinally();
      _jspx_tagPool_c_forEach_var_items.reuse(_jspx_th_c_forEach_0);
    }
    return false;
  }

  private boolean _jspx_meth_c_forEach_1(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:forEach
    org.apache.taglibs.standard.tag.rt.core.ForEachTag _jspx_th_c_forEach_1 = (org.apache.taglibs.standard.tag.rt.core.ForEachTag) _jspx_tagPool_c_forEach_var_items.get(org.apache.taglibs.standard.tag.rt.core.ForEachTag.class);
    _jspx_th_c_forEach_1.setPageContext(_jspx_page_context);
    _jspx_th_c_forEach_1.setParent(null);
    _jspx_th_c_forEach_1.setItems((java.lang.Object) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${requestScope.lsNotification}", java.lang.Object.class, (PageContext)_jspx_page_context, null));
    _jspx_th_c_forEach_1.setVar("noti");
    int[] _jspx_push_body_count_c_forEach_1 = new int[] { 0 };
    try {
      int _jspx_eval_c_forEach_1 = _jspx_th_c_forEach_1.doStartTag();
      if (_jspx_eval_c_forEach_1 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
        do {
          out.write("\r\n");
          out.write("                                <div class=\"notifi-item\">\r\n");
          out.write("                                <img src=\"https://p.kindpng.com/picc/s/78-785827_user-profile-avatar-login-account-male-user-icon.png\" alt=\"img\">\r\n");
          out.write("                                <div class=\"text\">\r\n");
          out.write("                                    <h4>Test Name</h4>\r\n");
          out.write("                                    <p>");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${noti.content}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("</p>\r\n");
          out.write("                                </div>\r\n");
          out.write("                            </div>\r\n");
          out.write("                            ");
          int evalDoAfterBody = _jspx_th_c_forEach_1.doAfterBody();
          if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
            break;
        } while (true);
      }
      if (_jspx_th_c_forEach_1.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        return true;
      }
    } catch (Throwable _jspx_exception) {
      while (_jspx_push_body_count_c_forEach_1[0]-- > 0)
        out = _jspx_page_context.popBody();
      _jspx_th_c_forEach_1.doCatch(_jspx_exception);
    } finally {
      _jspx_th_c_forEach_1.doFinally();
      _jspx_tagPool_c_forEach_var_items.reuse(_jspx_th_c_forEach_1);
    }
    return false;
  }

  private boolean _jspx_meth_c_choose_0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:choose
    org.apache.taglibs.standard.tag.common.core.ChooseTag _jspx_th_c_choose_0 = (org.apache.taglibs.standard.tag.common.core.ChooseTag) _jspx_tagPool_c_choose.get(org.apache.taglibs.standard.tag.common.core.ChooseTag.class);
    _jspx_th_c_choose_0.setPageContext(_jspx_page_context);
    _jspx_th_c_choose_0.setParent(null);
    int _jspx_eval_c_choose_0 = _jspx_th_c_choose_0.doStartTag();
    if (_jspx_eval_c_choose_0 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("\r\n");
        out.write("                ");
        if (_jspx_meth_c_when_0((javax.servlet.jsp.tagext.JspTag) _jspx_th_c_choose_0, _jspx_page_context))
          return true;
        out.write("\r\n");
        out.write("                ");
        if (_jspx_meth_c_otherwise_0((javax.servlet.jsp.tagext.JspTag) _jspx_th_c_choose_0, _jspx_page_context))
          return true;
        out.write("\r\n");
        out.write("            ");
        int evalDoAfterBody = _jspx_th_c_choose_0.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_c_choose_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_choose.reuse(_jspx_th_c_choose_0);
      return true;
    }
    _jspx_tagPool_c_choose.reuse(_jspx_th_c_choose_0);
    return false;
  }

  private boolean _jspx_meth_c_when_0(javax.servlet.jsp.tagext.JspTag _jspx_th_c_choose_0, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:when
    org.apache.taglibs.standard.tag.rt.core.WhenTag _jspx_th_c_when_0 = (org.apache.taglibs.standard.tag.rt.core.WhenTag) _jspx_tagPool_c_when_test.get(org.apache.taglibs.standard.tag.rt.core.WhenTag.class);
    _jspx_th_c_when_0.setPageContext(_jspx_page_context);
    _jspx_th_c_when_0.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_choose_0);
    _jspx_th_c_when_0.setTest(((java.lang.Boolean) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${sessionScope.user != null}", java.lang.Boolean.class, (PageContext)_jspx_page_context, null)).booleanValue());
    int _jspx_eval_c_when_0 = _jspx_th_c_when_0.doStartTag();
    if (_jspx_eval_c_when_0 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("\r\n");
        out.write("                    <div class=\"dropdown\" style=\"display: inline-block\">\r\n");
        out.write("                        <a\r\n");
        out.write("                            href=\"\"\r\n");
        out.write("                            class=\"login dropdown-toggle user-action\"\r\n");
        out.write("                            data-toggle=\"dropdown\"\r\n");
        out.write("                            >\r\n");
        out.write("                            ");
        out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${sessionScope.user}", java.lang.String.class, (PageContext)_jspx_page_context, null));
        out.write(" <b class=\"carpet\"></b>\r\n");
        out.write("                        </a>\r\n");
        out.write("                        <ul class=\"dropdown-menu\" style=\"width: 100px\">\r\n");
        out.write("                            <li>\r\n");
        out.write("                                <a\r\n");
        out.write("                                    href=\"UserProfileServlet?accountId=");
        out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${sessionScope.accountId}", java.lang.String.class, (PageContext)_jspx_page_context, null));
        out.write("\"\r\n");
        out.write("                                    >\r\n");
        out.write("                                    <i class=\"fa fa-user\"></i> Profile</a\r\n");
        out.write("                                >\r\n");
        out.write("                            </li>\r\n");
        out.write("                            <li>\r\n");
        out.write("                                <a\r\n");
        out.write("                                    href=\"UserBookMarkServlet?accountId=");
        out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${sessionScope.accountId}", java.lang.String.class, (PageContext)_jspx_page_context, null));
        out.write("\"\r\n");
        out.write("                                    >\r\n");
        out.write("                                    <i class=\"fa fa-bookmark\"></i> Book Mark</a\r\n");
        out.write("                                >\r\n");
        out.write("                            </li>\r\n");
        out.write("                            <li>\r\n");
        out.write("                                <a\r\n");
        out.write("                                    href=\"UserHistoryServlet?accountId=");
        out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${sessionScope.accountId}", java.lang.String.class, (PageContext)_jspx_page_context, null));
        out.write("\"\r\n");
        out.write("                                    >\r\n");
        out.write("                                    <i class=\"fa fa-history\"></i> Recently</a\r\n");
        out.write("                                >\r\n");
        out.write("                            </li>\r\n");
        out.write("                            <li>\r\n");
        out.write("                                <a href=\"UserFollowServlet?accountId=");
        out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${sessionScope.accountId}", java.lang.String.class, (PageContext)_jspx_page_context, null));
        out.write("\">\r\n");
        out.write("                                    <i class=\"fa fa-bookmark\"></i> Following</a\r\n");
        out.write("                                >\r\n");
        out.write("                            </li>\r\n");
        out.write("                            <li class=\"divider\"></li>\r\n");
        out.write("                            <li><a href=\"LogoutServlet\">Log Out</a></li>\r\n");
        out.write("                        </ul>\r\n");
        out.write("                    </div>\r\n");
        out.write("                ");
        int evalDoAfterBody = _jspx_th_c_when_0.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_c_when_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_when_test.reuse(_jspx_th_c_when_0);
      return true;
    }
    _jspx_tagPool_c_when_test.reuse(_jspx_th_c_when_0);
    return false;
  }

  private boolean _jspx_meth_c_otherwise_0(javax.servlet.jsp.tagext.JspTag _jspx_th_c_choose_0, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:otherwise
    org.apache.taglibs.standard.tag.common.core.OtherwiseTag _jspx_th_c_otherwise_0 = (org.apache.taglibs.standard.tag.common.core.OtherwiseTag) _jspx_tagPool_c_otherwise.get(org.apache.taglibs.standard.tag.common.core.OtherwiseTag.class);
    _jspx_th_c_otherwise_0.setPageContext(_jspx_page_context);
    _jspx_th_c_otherwise_0.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_choose_0);
    int _jspx_eval_c_otherwise_0 = _jspx_th_c_otherwise_0.doStartTag();
    if (_jspx_eval_c_otherwise_0 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("\r\n");
        out.write("                    <a href=\"login2.jsp\"><p class=\"login\">Login</p></a>\r\n");
        out.write("                ");
        int evalDoAfterBody = _jspx_th_c_otherwise_0.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_c_otherwise_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_otherwise.reuse(_jspx_th_c_otherwise_0);
      return true;
    }
    _jspx_tagPool_c_otherwise.reuse(_jspx_th_c_otherwise_0);
    return false;
  }

  private boolean _jspx_meth_c_forEach_2(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:forEach
    org.apache.taglibs.standard.tag.rt.core.ForEachTag _jspx_th_c_forEach_2 = (org.apache.taglibs.standard.tag.rt.core.ForEachTag) _jspx_tagPool_c_forEach_var_items.get(org.apache.taglibs.standard.tag.rt.core.ForEachTag.class);
    _jspx_th_c_forEach_2.setPageContext(_jspx_page_context);
    _jspx_th_c_forEach_2.setParent(null);
    _jspx_th_c_forEach_2.setItems((java.lang.Object) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${requestScope.categories}", java.lang.Object.class, (PageContext)_jspx_page_context, null));
    _jspx_th_c_forEach_2.setVar("cate");
    int[] _jspx_push_body_count_c_forEach_2 = new int[] { 0 };
    try {
      int _jspx_eval_c_forEach_2 = _jspx_th_c_forEach_2.doStartTag();
      if (_jspx_eval_c_forEach_2 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
        do {
          out.write("\r\n");
          out.write("                                            <a\r\n");
          out.write("                                                href=\"BookWithCategoryServlet?categoryId=");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${cate.categoryId}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("\"\r\n");
          out.write("                                                >");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${cate.categoryName}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("</a\r\n");
          out.write("                                            >\r\n");
          out.write("                                        ");
          int evalDoAfterBody = _jspx_th_c_forEach_2.doAfterBody();
          if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
            break;
        } while (true);
      }
      if (_jspx_th_c_forEach_2.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        return true;
      }
    } catch (Throwable _jspx_exception) {
      while (_jspx_push_body_count_c_forEach_2[0]-- > 0)
        out = _jspx_page_context.popBody();
      _jspx_th_c_forEach_2.doCatch(_jspx_exception);
    } finally {
      _jspx_th_c_forEach_2.doFinally();
      _jspx_tagPool_c_forEach_var_items.reuse(_jspx_th_c_forEach_2);
    }
    return false;
  }

  private boolean _jspx_meth_c_forEach_3(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:forEach
    org.apache.taglibs.standard.tag.rt.core.ForEachTag _jspx_th_c_forEach_3 = (org.apache.taglibs.standard.tag.rt.core.ForEachTag) _jspx_tagPool_c_forEach_var_items.get(org.apache.taglibs.standard.tag.rt.core.ForEachTag.class);
    _jspx_th_c_forEach_3.setPageContext(_jspx_page_context);
    _jspx_th_c_forEach_3.setParent(null);
    _jspx_th_c_forEach_3.setItems((java.lang.Object) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${requestScope.lsChapter}", java.lang.Object.class, (PageContext)_jspx_page_context, null));
    _jspx_th_c_forEach_3.setVar("chapter");
    int[] _jspx_push_body_count_c_forEach_3 = new int[] { 0 };
    try {
      int _jspx_eval_c_forEach_3 = _jspx_th_c_forEach_3.doStartTag();
      if (_jspx_eval_c_forEach_3 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
        do {
          out.write("\r\n");
          out.write("                                            <tr>\r\n");
          out.write("                                                <td>\r\n");
          out.write("                                                    <a\r\n");
          out.write("                                                        href=\"ChapterDetailServlet?bookId=");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${chapter.bookId}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("&chapterId=");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${chapter.chapterId}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("&chapterName=");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${chapter.chapterName}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("\"\r\n");
          out.write("                                                        >\r\n");
          out.write("                                                        ");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${chapter.chapterName}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("</a\r\n");
          out.write("                                                    >\r\n");
          out.write("                                                </td>\r\n");
          out.write("                                                <td>5 star</td>\r\n");
          out.write("                                                <td>");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${chapter.view}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("</td>\r\n");
          out.write("                                                <td>");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${chapter.getDate()}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("</td>\r\n");
          out.write("                                            </tr>\r\n");
          out.write("                                        ");
          int evalDoAfterBody = _jspx_th_c_forEach_3.doAfterBody();
          if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
            break;
        } while (true);
      }
      if (_jspx_th_c_forEach_3.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        return true;
      }
    } catch (Throwable _jspx_exception) {
      while (_jspx_push_body_count_c_forEach_3[0]-- > 0)
        out = _jspx_page_context.popBody();
      _jspx_th_c_forEach_3.doCatch(_jspx_exception);
    } finally {
      _jspx_th_c_forEach_3.doFinally();
      _jspx_tagPool_c_forEach_var_items.reuse(_jspx_th_c_forEach_3);
    }
    return false;
  }

  private boolean _jspx_meth_c_choose_1(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:choose
    org.apache.taglibs.standard.tag.common.core.ChooseTag _jspx_th_c_choose_1 = (org.apache.taglibs.standard.tag.common.core.ChooseTag) _jspx_tagPool_c_choose.get(org.apache.taglibs.standard.tag.common.core.ChooseTag.class);
    _jspx_th_c_choose_1.setPageContext(_jspx_page_context);
    _jspx_th_c_choose_1.setParent(null);
    int _jspx_eval_c_choose_1 = _jspx_th_c_choose_1.doStartTag();
    if (_jspx_eval_c_choose_1 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("\r\n");
        out.write("                ");
        if (_jspx_meth_c_when_1((javax.servlet.jsp.tagext.JspTag) _jspx_th_c_choose_1, _jspx_page_context))
          return true;
        out.write("\r\n");
        out.write("                ");
        if (_jspx_meth_c_otherwise_1((javax.servlet.jsp.tagext.JspTag) _jspx_th_c_choose_1, _jspx_page_context))
          return true;
        out.write("\r\n");
        out.write("            ");
        int evalDoAfterBody = _jspx_th_c_choose_1.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_c_choose_1.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_choose.reuse(_jspx_th_c_choose_1);
      return true;
    }
    _jspx_tagPool_c_choose.reuse(_jspx_th_c_choose_1);
    return false;
  }

  private boolean _jspx_meth_c_when_1(javax.servlet.jsp.tagext.JspTag _jspx_th_c_choose_1, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:when
    org.apache.taglibs.standard.tag.rt.core.WhenTag _jspx_th_c_when_1 = (org.apache.taglibs.standard.tag.rt.core.WhenTag) _jspx_tagPool_c_when_test.get(org.apache.taglibs.standard.tag.rt.core.WhenTag.class);
    _jspx_th_c_when_1.setPageContext(_jspx_page_context);
    _jspx_th_c_when_1.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_choose_1);
    _jspx_th_c_when_1.setTest(((java.lang.Boolean) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${sessionScope.user!=null}", java.lang.Boolean.class, (PageContext)_jspx_page_context, null)).booleanValue());
    int _jspx_eval_c_when_1 = _jspx_th_c_when_1.doStartTag();
    if (_jspx_eval_c_when_1 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("\r\n");
        out.write("                    <div class=\"post-comment mb-3\">\r\n");
        out.write("                        <div accept-charset=\"Windows-1256\" method=\"post\">\r\n");
        out.write("                            <div class=\"post-comment-content\">\r\n");
        out.write("                                <textarea\r\n");
        out.write("                                    id=\"comment\"\r\n");
        out.write("                                    style=\"\r\n");
        out.write("                                    background-color: rgba(103, 104, 105, 0.3);\r\n");
        out.write("                                    width: 100%;\r\n");
        out.write("                                    color: black;\r\n");
        out.write("                                    border: 1px solid gray;\r\n");
        out.write("                                    \"\r\n");
        out.write("                                    rows=\"5\"\r\n");
        out.write("                                    name=\"comment\"\r\n");
        out.write("                                    >\r\n");
        out.write("                                    Type your comment here</textarea\r\n");
        out.write("                                >\r\n");
        out.write("                            </div>\r\n");
        out.write("                            <div\r\n");
        out.write("                                class=\"post-comment-submit\"\r\n");
        out.write("                                style=\"background-color: #292926; height: 40px\"\r\n");
        out.write("                                >\r\n");
        out.write("                                <button\r\n");
        out.write("                                    style=\"\r\n");
        out.write("                                    margin: 3px 10px 0;\r\n");
        out.write("                                    color: white;\r\n");
        out.write("                                    background-color: #36a189;\r\n");
        out.write("                                    padding: 2px 15px;\r\n");
        out.write("                                    border-radius: 5px;\r\n");
        out.write("                                    font-weight: 700;\r\n");
        out.write("                                    border-width: 0px;\r\n");
        out.write("                                    border: none;\r\n");
        out.write("                                    \"\r\n");
        out.write("                                    onclick=\"loadComment()\"\r\n");
        out.write("                                    >\r\n");
        out.write("                                    Post Comment\r\n");
        out.write("                                </button>\r\n");
        out.write("                            </div>\r\n");
        out.write("                        </div>\r\n");
        out.write("                    </div>\r\n");
        out.write("                ");
        int evalDoAfterBody = _jspx_th_c_when_1.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_c_when_1.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_when_test.reuse(_jspx_th_c_when_1);
      return true;
    }
    _jspx_tagPool_c_when_test.reuse(_jspx_th_c_when_1);
    return false;
  }

  private boolean _jspx_meth_c_otherwise_1(javax.servlet.jsp.tagext.JspTag _jspx_th_c_choose_1, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:otherwise
    org.apache.taglibs.standard.tag.common.core.OtherwiseTag _jspx_th_c_otherwise_1 = (org.apache.taglibs.standard.tag.common.core.OtherwiseTag) _jspx_tagPool_c_otherwise.get(org.apache.taglibs.standard.tag.common.core.OtherwiseTag.class);
    _jspx_th_c_otherwise_1.setPageContext(_jspx_page_context);
    _jspx_th_c_otherwise_1.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_choose_1);
    int _jspx_eval_c_otherwise_1 = _jspx_th_c_otherwise_1.doStartTag();
    if (_jspx_eval_c_otherwise_1 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("\r\n");
        out.write("                    <div class=\"post-comment mb-3\" style=\"color: white\">\r\n");
        out.write("                        You must login to post your comment\r\n");
        out.write("                    </div>\r\n");
        out.write("                ");
        int evalDoAfterBody = _jspx_th_c_otherwise_1.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_c_otherwise_1.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_otherwise.reuse(_jspx_th_c_otherwise_1);
      return true;
    }
    _jspx_tagPool_c_otherwise.reuse(_jspx_th_c_otherwise_1);
    return false;
  }

  private boolean _jspx_meth_c_forEach_4(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:forEach
    org.apache.taglibs.standard.tag.rt.core.ForEachTag _jspx_th_c_forEach_4 = (org.apache.taglibs.standard.tag.rt.core.ForEachTag) _jspx_tagPool_c_forEach_var_items.get(org.apache.taglibs.standard.tag.rt.core.ForEachTag.class);
    _jspx_th_c_forEach_4.setPageContext(_jspx_page_context);
    _jspx_th_c_forEach_4.setParent(null);
    _jspx_th_c_forEach_4.setItems((java.lang.Object) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${requestScope.lsComment}", java.lang.Object.class, (PageContext)_jspx_page_context, null));
    _jspx_th_c_forEach_4.setVar("comment");
    int[] _jspx_push_body_count_c_forEach_4 = new int[] { 0 };
    try {
      int _jspx_eval_c_forEach_4 = _jspx_th_c_forEach_4.doStartTag();
      if (_jspx_eval_c_forEach_4 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
        do {
          out.write("\r\n");
          out.write("            <div id =\"new-comment\" class=\"col-md-12\"></div>\r\n");
          out.write("            <div class=\"col-md-12\">\r\n");
          out.write("                <button type=\"button\" class=\"btn btn-danger dropdown-toggle delete2\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\r\n");
          out.write("                    Report this comment!\r\n");
          out.write("                </button>\r\n");
          out.write("                <div class=\"dropdown-menu book-report\">\r\n");
          out.write("                    <div class=\"book-report-content\" style=\"display:none\"></div>\r\n");
          out.write("                    <div class=\"dropdown-item\" >Abusive content</div>\r\n");
          out.write("                    <div class=\"dropdown-item\" >Plagiarism</div>\r\n");
          out.write("                    <div class=\"dropdown-item\" >Erotic content</div>\r\n");
          out.write("                    <div class=\"dropdown-item\" >Hate speech</div>\r\n");
          out.write("\r\n");
          out.write("                </div>\r\n");
          out.write("                <div class=\"media g-mb-30 media-comment\">\r\n");
          out.write("                    <img\r\n");
          out.write("                        class=\"d-flex g-width-50 g-height-50 rounded-circle g-mt-3 g-mr-15\"\r\n");
          out.write("                        src=\"asset/img/body/undraw_profile_pic_ic5t.svg\"\r\n");
          out.write("                        alt=\"Image Description\"\r\n");
          out.write("                        />\r\n");
          out.write("                    <div class=\"media-body u-shadow-v18 g-bg-secondary g-pa-30\">\r\n");
          out.write("                        <div class=\"g-mb-15\">\r\n");
          out.write("                            <h5 class=\"h5 g-color-gray-dark-v1 mb-0\">\r\n");
          out.write("                                ");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${comment.account.name}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("\r\n");
          out.write("                            </h5>\r\n");
          out.write("                            <span class=\"g-color-gray-dark-v4 g-font-size-12\"\r\n");
          out.write("                                  >");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${comment.getPostTime()}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("</span\r\n");
          out.write("                            >\r\n");
          out.write("                        </div>\r\n");
          out.write("\r\n");
          out.write("                        <p>");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${comment.content}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("</p>\r\n");
          out.write("\r\n");
          out.write("                        <ul class=\"list-inline d-sm-flex my-0\">\r\n");
          out.write("                            <li class=\"list-inline-item g-mr-20\">\r\n");
          out.write("                                <a\r\n");
          out.write("                                    class=\"u-link-v5 g-color-gray-dark-v4 g-color-primary--hover\"\r\n");
          out.write("                                    href=\"#!\"\r\n");
          out.write("                                    >\r\n");
          out.write("                                    <i class=\"fa fa-thumbs-up g-pos-rel g-top-1 g-mr-3\"></i>\r\n");
          out.write("                                    178\r\n");
          out.write("                                </a>\r\n");
          out.write("                            </li>\r\n");
          out.write("                            <li class=\"list-inline-item g-mr-20\">\r\n");
          out.write("                                <a\r\n");
          out.write("                                    class=\"u-link-v5 g-color-gray-dark-v4 g-color-primary--hover\"\r\n");
          out.write("                                    href=\"#!\"\r\n");
          out.write("                                    >\r\n");
          out.write("                                    <i class=\"fa fa-thumbs-down g-pos-rel g-top-1 g-mr-3\"></i>\r\n");
          out.write("                                    34\r\n");
          out.write("                                </a>\r\n");
          out.write("                            </li>\r\n");
          out.write("                            <li class=\"list-inline-item ml-auto\">\r\n");
          out.write("                                <a\r\n");
          out.write("                                    class=\"u-link-v5 g-color-gray-dark-v4 g-color-primary--hover\"\r\n");
          out.write("                                    href=\"#!\"\r\n");
          out.write("                                    >\r\n");
          out.write("                                    <i class=\"fa fa-reply g-pos-rel g-top-1 g-mr-3\"></i>\r\n");
          out.write("                                    Reply\r\n");
          out.write("                                </a>\r\n");
          out.write("                            </li>\r\n");
          out.write("                        </ul>\r\n");
          out.write("                    </div>\r\n");
          out.write("                </div>\r\n");
          out.write("            </div>\r\n");
          out.write("        ");
          int evalDoAfterBody = _jspx_th_c_forEach_4.doAfterBody();
          if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
            break;
        } while (true);
      }
      if (_jspx_th_c_forEach_4.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        return true;
      }
    } catch (Throwable _jspx_exception) {
      while (_jspx_push_body_count_c_forEach_4[0]-- > 0)
        out = _jspx_page_context.popBody();
      _jspx_th_c_forEach_4.doCatch(_jspx_exception);
    } finally {
      _jspx_th_c_forEach_4.doFinally();
      _jspx_tagPool_c_forEach_var_items.reuse(_jspx_th_c_forEach_4);
    }
    return false;
  }
}
