package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class publisherNavbar_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta charset=\"UTF-8\" />\n");
      out.write("        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\" />\n");
      out.write("        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" />\n");
      out.write("        <title>Home Page</title>\n");
      out.write("        <link rel=\"stylesheet\" href=\"asset/css/bootstrap.css\" />\n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("        <nav class=\"navbar navbar-dark bg-dark\">\n");
      out.write("            <a href=\"CreateTablePublisher/Book\" class=\"navbar-brand text-light bg-dark text-center\">\n");
      out.write("                Bro<span class=\"text-dark bg-warning\">mic</span>\n");
      out.write("            </a>\n");
      out.write("            <a href=\"BookServlet\" class=\"navbar-nav text-light mr-auto\n");
      out.write("               \">\n");
      out.write("                Go To Reader Page\n");
      out.write("            </a>           \n");
      out.write("            <div class=\"right\">\n");
      out.write("                <script>\n");
      out.write("                    var box = document.getElementById('box');\n");
      out.write("                    var down = false;\n");
      out.write("\n");
      out.write("                    function toggleNotification() {\n");
      out.write("                        if (down) {\n");
      out.write("                            box.style.height = '0px';\n");
      out.write("                            box.style.opacity = 0;\n");
      out.write("                            down = false;\n");
      out.write("                        } else {\n");
      out.write("                            box.style.height = '510px';\n");
      out.write("                            box.style.opacity = 1;\n");
      out.write("                            down = true;\n");
      out.write("                        }\n");
      out.write("                    }\n");
      out.write("                </script>\n");
      out.write("\n");
      out.write("                <c:choose>\n");
      out.write("                    <c:when test=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${sessionScope.user != null}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("\">\n");
      out.write("                        <div class=\"dropdown\" style=\"display: inline-block\">\n");
      out.write("                            <a\n");
      out.write("                                href=\"\"\n");
      out.write("                                class=\"login dropdown-toggle user-action\"\n");
      out.write("                                data-toggle=\"dropdown\"\n");
      out.write("                                >\n");
      out.write("                                ");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${sessionScope.user}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write(" <b class=\"carpet\"></b>\n");
      out.write("                            </a>\n");
      out.write("                            <ul class=\"dropdown-menu\" style=\"width: 100px\">\n");
      out.write("                                <li>\n");
      out.write("                                    <a\n");
      out.write("                                        href=\"UserProfileServlet?accountId=");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${sessionScope.accountId}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("\"\n");
      out.write("                                        >\n");
      out.write("                                        <i class=\"fa fa-user\"></i> Profile</a\n");
      out.write("                                    >\n");
      out.write("                                </li>\n");
      out.write("                                <li>\n");
      out.write("                                    <a\n");
      out.write("                                        href=\"CreateTablePublisher/Book\"\n");
      out.write("                                        >\n");
      out.write("                                        <i class=\"fa fa-bookmark\"></i> Go to Work Space</a\n");
      out.write("                                    >\n");
      out.write("                                </li>\n");
      out.write("                                <li>\n");
      out.write("                                    <a\n");
      out.write("                                        href=\"UserHistoryServlet?accountId=");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${sessionScope.accountId}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("\"\n");
      out.write("                                        >\n");
      out.write("                                        <i class=\"fa fa-history\"></i> Recently</a\n");
      out.write("                                    >\n");
      out.write("                                </li>\n");
      out.write("                                <li>\n");
      out.write("                                    <a href=\"UserFollowServlet?accountId=");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${sessionScope.accountId}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("\">\n");
      out.write("                                        <i class=\"fa fa-bookmark\"></i> Following</a\n");
      out.write("                                    >\n");
      out.write("                                </li>\n");
      out.write("                                <li class=\"divider\"></li>\n");
      out.write("                                <li><a href=\"LogoutServlet\">Log Out</a></li>\n");
      out.write("                            </ul>\n");
      out.write("                        </div>\n");
      out.write("                    </c:when>\n");
      out.write("                    <c:otherwise>\n");
      out.write("                        <a href=\"login2.jsp\"><p class=\"login\">Login</p></a>\n");
      out.write("                    </c:otherwise>\n");
      out.write("                </c:choose>\n");
      out.write("            </div>\n");
      out.write("        </nav>\n");
      out.write("    </body>\n");
      out.write("    <script src=\"asset/js/popper.js\"></script>\n");
      out.write("    <script type=\"text/javascript\">\n");
      out.write("                    $(\".off\").click(function () {\n");
      out.write("                        $(this).toggleClass(\"on\");\n");
      out.write("                    });\n");
      out.write("    </script>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
