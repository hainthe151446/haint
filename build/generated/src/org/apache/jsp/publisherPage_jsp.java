package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import entity.Translator;
import entity.Book;
import Thai.entity.Report;
import java.util.List;
import entity.Account;
import dao.TranslatorDao;
import dao.Thai.TReportDao;
import dao.BookDao;
import dao.Thai.TAccountDao;

public final class publisherPage_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_c_forEach_var_items;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _jspx_tagPool_c_forEach_var_items = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
  }

  public void _jspDestroy() {
    _jspx_tagPool_c_forEach_var_items.release();
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<!DOCTYPE html>\r\n");
      out.write("<!DOCTYPE html>\r\n");
      out.write("<html lang=\"en\">\r\n");
      out.write("    <head>\r\n");
      out.write("        <meta charset=\"UTF-8\" />\r\n");
      out.write("        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\" />\r\n");
      out.write("        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" />\r\n");
      out.write("        <link rel=\"stylesheet\" href=\"asset/css/bootstrap.css\" />\r\n");
      out.write("        <link rel=\"stylesheet\" href=\"asset/css/publisher.css\" />\r\n");
      out.write("        <title>Document</title>\r\n");
      out.write("    </head>\r\n");
      out.write("    <body>\r\n");
      out.write("        <div class=\"wrapper\">\r\n");
      out.write("            <!-- Sidebar  -->\r\n");
      out.write("            <nav id=\"sidebar\">\r\n");
      out.write("                <div class=\"sidebar-header\"  style=\"background: #fbd593;\">\r\n");
      out.write("                    <a href=\"BookServlet\"><h3>BROMICS</h3></a>\r\n");
      out.write("                </div>\r\n");
      out.write("\r\n");
      out.write("                <ul class=\"list-unstyled components\" style=\"background: #FDE8C4;\">\r\n");
      out.write("                    <li>\r\n");
      out.write("                        <a href=\"#\">Dashboard</a>\r\n");
      out.write("                    </li>\r\n");
      out.write("                    <li>\r\n");
      out.write("                        <a href=\"TableServlet/Account\">Acount Management</a>\r\n");
      out.write("                    </li>\r\n");
      out.write("                    <li>\r\n");
      out.write("                        <a href=\"TableServlet/Book\">Book Management</a>\r\n");
      out.write("                    </li>\r\n");
      out.write("                    <li>\r\n");
      out.write("                        <a href=\"TableServlet/Translator\">Translator Management</a>\r\n");
      out.write("                    </li>\r\n");
      out.write("                    <li>\r\n");
      out.write("                        <a href=\"TableServlet/Report\">Report Management</a>\r\n");
      out.write("                    </li>\r\n");
      out.write("                </ul>\r\n");
      out.write("            </nav>\r\n");
      out.write("\r\n");
      out.write("            <!-- Page Content  -->\r\n");
      out.write("            <div id=\"content\">\r\n");
      out.write("                <nav class=\"navbar navbar-expand-lg navbar-light bg-light\">\r\n");
      out.write("                    <div class=\"container-fluid\">\r\n");
      out.write("                        <button type=\"button\" id=\"sidebarCollapse\" class=\"btn btn-info\"  style=\"background: #FDCBC4; border-color: #FDC4F5\">\r\n");
      out.write("                            <i class=\"fas fa-align-left\"></i>\r\n");
      out.write("                            <span>Toggle Sidebar</span>\r\n");
      out.write("                        </button>\r\n");
      out.write("                        <form class=\"d-flex\" action=\"AdminSearch\">\r\n");
      out.write("                            <select class=\"form-select\" aria-label=\"Default select example\" name=\"searchType\">\r\n");
      out.write("                                <option value=\"0\" selected>Search Item</option>\r\n");
      out.write("                                <option value=\"Account\">Account</option>\r\n");
      out.write("                                <option value=\"Book\">Book</option>\r\n");
      out.write("                                <option value=\"Translator\">Translator</option>\r\n");
      out.write("                                <option value=\"Report\">Report</option>\r\n");
      out.write("                            </select>\r\n");
      out.write("                            <input class=\"form-control me-2\" type=\"search\" placeholder=\"Search\" aria-label=\"Search\" name=\"searchValue\">\r\n");
      out.write("                            <button class=\"btn btn-outline-dark\" type=\"submit\">Search</button>\r\n");
      out.write("                        </form>\r\n");
      out.write("                        <button\r\n");
      out.write("                            class=\"btn btn-dark d-inline-block d-lg-none ml-auto\"\r\n");
      out.write("                            type=\"button\"\r\n");
      out.write("                            data-toggle=\"collapse\"\r\n");
      out.write("                            data-target=\"#navbarSupportedContent\"\r\n");
      out.write("                            aria-controls=\"navbarSupportedContent\"\r\n");
      out.write("                            aria-expanded=\"false\"\r\n");
      out.write("                            aria-label=\"Toggle navigation\"\r\n");
      out.write("                            >\r\n");
      out.write("                            <i class=\"fas fa-align-justify\"></i>\r\n");
      out.write("                        </button>\r\n");
      out.write("\r\n");
      out.write("                    </div>\r\n");
      out.write("                </nav>\r\n");
      out.write("\r\n");
      out.write("                <button type=\"button\" class=\"btn btn-outline-warning\" onclick=\"myFunction2()\">Add</button>\r\n");
      out.write("\r\n");
      out.write("                <div id=\"myModal2\" class=\"hmodal\" style=\"text-align: left;\">\r\n");
      out.write("                    <!-- Modal content -->\r\n");
      out.write("                    <div class=\"hmodal-content\">\r\n");
      out.write("                        <div class=\"hmodal-header\">\r\n");
      out.write("                            <span class=\"close2\">&times;</span>\r\n");
      out.write("                            <h2>Modal Header</h2>\r\n");
      out.write("                        </div>\r\n");
      out.write("                        <form action=\"AddChapterServlet\">\r\n");
      out.write("                            <div class=\"hmodal-body\">\r\n");
      out.write("                                <p style=\"display: inline;\">Comic: </p>\r\n");
      out.write("                                \r\n");
      out.write("                                <select name = \"bookId\" class=\"form-control\" id=\"exampleFormControlSelect1\" style=\"display: inline; width: 50%\">\r\n");
      out.write("                                    ");
      if (_jspx_meth_c_forEach_0(_jspx_page_context))
        return;
      out.write("\r\n");
      out.write("                                </select>\r\n");
      out.write("                                <p style=\"display: block;\"></p>\r\n");
      out.write("                                <p style=\"display: inline;\">Chapter: </p><input id=\"b2\" name=\"chapterName\">\r\n");
      out.write("                                <p></p>\r\n");
      out.write("                            </div>\r\n");
      out.write("                            <div class=\"hmodal-footer\">\r\n");
      out.write("                                <button class=\"btn btn-outline-dark\" type=\"submit\" id=\"savebtn\">Add</button>\r\n");
      out.write("                            </div>\r\n");
      out.write("                        </form>\r\n");
      out.write("                    </div>\r\n");
      out.write("                </div>\r\n");
      out.write("\r\n");
      out.write("                <script>\r\n");
      out.write("// Get the modal\r\n");
      out.write("                    var modal2 = document.getElementById(\"myModal2\");\r\n");
      out.write("\r\n");
      out.write("// Get the <span> element that closes the modal\r\n");
      out.write("                    var span2 = document.getElementsByClassName(\"close2\")[0];\r\n");
      out.write("\r\n");
      out.write("                    var btn = document.getElementById(\"savebtn\");\r\n");
      out.write("\r\n");
      out.write("// When the user clicks the button, open the modal \r\n");
      out.write("\r\n");
      out.write("                    function myFunction2(a, b, c, d, e) {\r\n");
      out.write("                        modal2.style.display = \"block\";\r\n");
      out.write("\r\n");
      out.write("                    }\r\n");
      out.write("\r\n");
      out.write("// When the user clicks on <span> (x), close the modal\r\n");
      out.write("\r\n");
      out.write("                    span2.onclick = function () {\r\n");
      out.write("                        modal2.style.display = \"none\";\r\n");
      out.write("                    }\r\n");
      out.write("\r\n");
      out.write("                    btn.onclick = function () {\r\n");
      out.write("                        modal2.style.display = \"none\";\r\n");
      out.write("                    }\r\n");
      out.write("\r\n");
      out.write("// When the user clicks anywhere outside of the modal, close it\r\n");
      out.write("                    window.onclick = function (event) {\r\n");
      out.write("                        if (event.target == modal2) {\r\n");
      out.write("                            modal.style.display = \"none\";\r\n");
      out.write("                            modal2.style.display = \"none\";\r\n");
      out.write("                        }\r\n");
      out.write("                    }\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("                </script>\r\n");
      out.write("            </div>\r\n");
      out.write("        </div>\r\n");
      out.write("        <script src=\"https://code.jquery.com/jquery-3.4.1.js\"></script>\r\n");
      out.write("        <script src=\"asset/js/bootstrap.js\"></script>\r\n");
      out.write("        <script>\r\n");
      out.write("                    $(document).ready(function () {\r\n");
      out.write("                        $(\"#sidebarCollapse\").on(\"click\", function () {\r\n");
      out.write("                            $(\"#sidebar\").toggleClass(\"active\");\r\n");
      out.write("                        });\r\n");
      out.write("                    });\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("        </script>\r\n");
      out.write("    </body>\r\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }

  private boolean _jspx_meth_c_forEach_0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:forEach
    org.apache.taglibs.standard.tag.rt.core.ForEachTag _jspx_th_c_forEach_0 = (org.apache.taglibs.standard.tag.rt.core.ForEachTag) _jspx_tagPool_c_forEach_var_items.get(org.apache.taglibs.standard.tag.rt.core.ForEachTag.class);
    _jspx_th_c_forEach_0.setPageContext(_jspx_page_context);
    _jspx_th_c_forEach_0.setParent(null);
    _jspx_th_c_forEach_0.setItems((java.lang.Object) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${requestScope.lsBook}", java.lang.Object.class, (PageContext)_jspx_page_context, null));
    _jspx_th_c_forEach_0.setVar("book");
    int[] _jspx_push_body_count_c_forEach_0 = new int[] { 0 };
    try {
      int _jspx_eval_c_forEach_0 = _jspx_th_c_forEach_0.doStartTag();
      if (_jspx_eval_c_forEach_0 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
        do {
          out.write("\r\n");
          out.write("                                        <option value=\"");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${book.id}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write('"');
          out.write('>');
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${book.name}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("</option>\r\n");
          out.write("                                    ");
          int evalDoAfterBody = _jspx_th_c_forEach_0.doAfterBody();
          if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
            break;
        } while (true);
      }
      if (_jspx_th_c_forEach_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        return true;
      }
    } catch (Throwable _jspx_exception) {
      while (_jspx_push_body_count_c_forEach_0[0]-- > 0)
        out = _jspx_page_context.popBody();
      _jspx_th_c_forEach_0.doCatch(_jspx_exception);
    } finally {
      _jspx_th_c_forEach_0.doFinally();
      _jspx_tagPool_c_forEach_var_items.reuse(_jspx_th_c_forEach_0);
    }
    return false;
  }
}
